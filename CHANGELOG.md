# Changelog

All notable changes to this project will be documented in this file.


## v0.0.1 — 2024-10-10

### Added

- bookmarks: implements legacy private XML based bookmarks
- compress: implements stream compression using GZIP or LZW
- privatexml: implements storage of arbitrary XML on the server
