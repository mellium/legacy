module mellium.im/legacy

go 1.22.0

toolchain go1.23.2

require (
	mellium.im/xmlstream v0.15.4
	mellium.im/xmpp v0.22.0
)

require (
	golang.org/x/crypto v0.28.0 // indirect
	golang.org/x/mod v0.21.0 // indirect
	golang.org/x/net v0.30.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/text v0.19.0 // indirect
	golang.org/x/tools v0.26.0 // indirect
	mellium.im/reader v0.1.0 // indirect
	mellium.im/sasl v0.3.2 // indirect
)
